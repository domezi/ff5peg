## DaVinci Resolve Linux
ff5peg converts your video files for use with resolve linux using ffmpeg.


### Installation
~~~
git clone https://gitlab.com/domezi/ff5peg.git && cd ff5peg && ./ff5peg -i
~~~

### How to use
Simple convert (from mp4 to mov)
~~~
./ff5peg 
~~~

Reverse convert (back to mp4, usually after resolve export)
~~~
./ff5peg 
~~~

### Bug reports
Please feel free to create issues at https://gitlab.com/domezi/ff5peg/issues